<!doctype html>
<html lang="ru">
<head>
    <meta charset="windows-1251">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="css/general.css">
    <title>Шифр RC4</title>
</head>
<body>
<div class="content">

</body>
</html>

<?php
    error_reporting(0);
    main();
    function main() {
        if($_POST['crypt'] === "ЗашифроватьИзФайла" || $_POST['crypt'] === "РасшифроватьИзФайла") {
            $_POST['inputMessage'] = file_get_contents('inputMessage.txt');
        }

        if($_POST['crypt'] === "Зашифровать" || $_POST['crypt'] === "ЗашифроватьИзФайла") {
            $_POST['result'] = encrypt($_POST['inputMessage'], $_POST['key']);
            file_put_contents('result.txt', $_POST['result']); // Запись результата в файл
        }

        if($_POST['crypt'] === "Расшифровать" || $_POST['crypt'] === "РасшифроватьИзФайла") {
            $_POST['result'] = decrypt($_POST['inputMessage'], $_POST['key']);
            file_put_contents('result.txt', $_POST['result']); // Запись результата в файл
        }

        show_result();
    }

    // Первый этап шифрования - это инициализация S-блока путем перестановок, определяемых ключем
    function init_s_block($key) {
        $k = unpack('C*', $key);
        array_unshift($k, array_shift($k));
        $n = sizeof($k);
        for ($i = $n; $i < 256; $i++) $k[$i] = $k[$i % $n];
        for ($i--; $i >= 256; $i--) $k[$i & 255] ^= $k[$i];

        $s = array();
        for ($i = 0; $i < 256; $i++) $s[$i] = $i;
        $j = 0;

        for ($i = 0; $i < 256; $i++) {
            $j = ($j + $s[$i] + $k[$i]) & 255;
            // перестановка $s[$i] и $s[$j]
            $tmp = $s[$i];
            $s[$i] = $s[$j];
            $s[$j] = $tmp;
        }
        return $s;
    }

    // Второй этап шифрования - это генерация псевдослучайной строки путем перестановок, хранящихся в S-блоке
    function crypts($text1, $key) {
        $s = init_s_block($key); //инициализация s блока
        $n = strlen($text1);
        $text2 = '';
        $i = $j = 0;
        for ($k = 0; $k < $n; $k++) {
            $i = ($i + 1) & 0xff;
            $j = ($j + $s[$i]) & 0xff;
            // перестановка $s[i] и $s[$j]
            $tmp = $s[$i];
            $s[$i] = $s[$j];
            $s[$j] = $tmp;
            $text2 .= $text1{$k} ^ chr($s[$i] + $s[$j]);
        }
        return $text2;
    }

    function encrypt($text, $password) {
        return base64_encode(crypts($text, $password));
    }

    function decrypt($enc_text, $password) {
        return crypts(base64_decode($enc_text), $password);
    }

    function show_result() {
        echo "<div class='area_small'>Cообщение: " . $_POST['inputMessage'] . "</div><br>";
        echo "<div class='area_small'>Ключ: " . $_POST['key'] . "</div><br>";
        echo "<div class='area_small'>Результат: " . $_POST['result'] . "</div><br>";
        echo "<a href='index.php'><input class='submit_btn' id='abs' type='submit' name='crypt' value='Вернуться на главную'></a>";
    }


