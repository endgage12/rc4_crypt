<!doctype html>
<html lang="ru">
<head>
    <meta charset="windows-1251">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="css/general.css">
    <title>Шифр RC4</title>
</head>
<body>
<div class="content">
    <form id='form' name='secretInfo' method='post' action='crypt.php'>
        <h1>RC4 шифрование</h1>
        <br>
        <textarea name='inputMessage' class="txt_holder" placeholder="Добро пожаловать, пожалуйста, введите сообщение"></textarea>
        <br>
        <label for='key'> <img src="https://img.icons8.com/cotton/64/000000/key--v1.png"/> Ключ</label>
        <input type='text' name='key' required>
        <br>

        <br>
        <input class="submit_btn" type='submit' name='crypt' value='Зашифровать'>
        <input class="submit_btn" type='submit' name='crypt' value='ЗашифроватьИзФайла'> <br>
        <input class="submit_btn"  type='submit' name='crypt' value='Расшифровать'>
        <input class="submit_btn" type='submit' name='crypt' value='РасшифроватьИзФайла'> <br> <br> <br>
    </form>
</body>
</html>